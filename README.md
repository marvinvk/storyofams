# Story of AMS test case #

## Running the app
To run the iOS app you need to have xCode installed and the you can run
```sh
react-native run-ios
```
You can run it on a specific device by using the ```--simulator=”iPhone X”``` flag.

To run it on Android, you need to open a simulator first. You can do this by opening the project in Android studio and press the play button. Then you can run:
```sh
react-native run-android
```
You can also run the production build version by adding the ```--variant=release``` flag.

## Building
For Android, the application gets built and distributed automatically.
* When pushing to the `develop` branch, a debug build will be made and pushed to Fabric.
* When pushing to `master`, a release will be built and pushed to the Play Store.

_note: remember to update your app's `versionCode`, otherwise the Play Store release will fail!_

Both builds will also be available for download from the GitLab CI/CD page.

**iOS requires a manual build (for now).**

The CI/CD pipeline makes use of a pre-built Docker container that contains all needed SDKs and dependencies.
To create the Android-build image:
```sh
cd builder
make build
```
and to run it locally, eg. for testing of the pipeline:
```sh
make start
```
Finally, to push the container to the registry:
```sh
make push
```

## Common React Native errors
### *“hasteImpl returning the same name for different files”*
Node things react-native is loaded twice, a solution is to change the “name” in ios/Pods/React/package.json.

### *"Apple Mach-O Linker Error” and/or “RCTBundleURLProvider.h” file not found - AppDelegate.m*
Delete node modules, then run npm install (or better yet yarn) and after everything has finished downloading, run react-native upgrade which should give you the option to replace old files with the template ones, by doing so you re-link your native dependencies in react-native which should fix your problem. Of course don't forget to clean your project in Xcode.

### *Unable to load script from assets ‘index.android.bundle’*
https://medium.com/@adityasingh_32512/solved-unable-to-load-script-from-assets-index-android-bundle-bdc5e3a3d5ff

### *“This must be a bug with + echo 'React Native”*
I’ve created a command for this: `npm run build:ios`
https://github.com/facebook/react-native/issues/15432#issuecomment-321980647

