module.exports = {
  root: true,
  // extends: '@react-native-community',
  rules: {
    quotes: ['error', 'single', { avoidEscape: true }],
    indent: ['error', 'tab'],
    'react/jsx-indent': ['error', 'tab'],
  }
};
