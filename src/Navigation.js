import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Easing } from 'react-native';

// styles
import Colors from './styles/Colors';

// screens
import HomeScreen from './screens/HomeScreen';
import PictureScreen from './screens/PictureScreen';

export const DefaultTransition = (index, position, layout) => {
	const translateX = position.interpolate({
		inputRange: [index - 1, index, index + 1],
		outputRange: [layout.initWidth, 0, 0]
	});

	return {
		transform: [{ translateX }],
	};
};

export const NoneTransition = () => {
	return {};
};

const AppNavigator = createStackNavigator({
	Home: {
		screen: HomeScreen,
		navigationOptions: {
			header: null,
		},
	},
	Picture: {
		screen: PictureScreen,
		navigationOptions: {
            header: null,
        },
	},
}, {
	transitionConfig: () => ({
		transitionSpec: {
			duration: 250,
			easing: Easing.inOut(Easing.ease),
			useNativeDriver: true,
			friction: 1,
    		tension: 0.5,
		},
		// Define scene interpolation, eq. custom transition
        screenInterpolator: (sceneProps) => {
			const { position, layout, scene } = sceneProps;
            const { index, route } = scene;
            const params = route.params || {};
            const transition = params.transition || 'default';

            return {
				default: DefaultTransition(index, position, layout),
				none: NoneTransition(),
			}[transition];
		}
	}),
	tabBarOptions: {
		inactiveTintColor: Colors.white,
		activeTintColor: Colors.white,
		activeBackgroundColor: Colors.white,
		style: {
			borderTopWidth: 0,
			backgroundColor: Colors.white,
		},
		iconStyle: {
			padding: 5,
			paddingBottom: 0,
		},
		labelStyle: {
			padding: 5,
			paddingTop: 0,
			fontSize: 12,
		},
	}
});

export default createAppContainer(AppNavigator);
