import React, { Component } from 'react';
import { StatusBar, View, ScrollView, RefreshControl, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/MaterialIcons';

// redux
import { getPicture } from '../redux/pictures';

// styles
import LayoutStyles from '../styles/Layout';
import TextStyles from '../styles/Text';
import Colors from '../styles/Colors';
import Sizes from '../styles/Sizes';

// components
import PictureTile from '../components/PictureTile';

export default connect(
	state => ({
		pictures: state.pictures,
	}), {
        getPicture,
    },
)(class PictureScreen extends Component {

	state = {
        isRefreshing: false,
    };

	componentDidMount() {
        const listingId = this.props.navigation.getParam('id', null);
        this.props.getPicture(listingId);
    }

    handleRefresh = () => {
        const listingId = this.props.navigation.getParam('id', null);
        this.props.getPicture(listingId).then(() => {
            this.setState({
                isRefreshing: false,
            });
        });
	};

	renderDetails(picture) {
        if (!picture.user || !picture.location) {
            return (
                <View style={[styles.details, styles.loaderHolder]}>
                    <ActivityIndicator size="small" color={Colors.primary} />
                </View>
            );
		}

		return (
			<View style={styles.details}>
				<View style={styles.detail}>
					<Text style={[TextStyles.p, styles.label]}>Downloads</Text>
					<Text style={[TextStyles.p, styles.value]}>{picture.downloads || 0}</Text>
				</View>
				<View style={styles.detail}>
					<Text style={[TextStyles.p, styles.label]}>Country</Text>
					<Text style={[TextStyles.p, styles.value]}>{picture.location.country || picture.user.location}</Text>
				</View>
				<View style={styles.detail}>
					<Text style={[TextStyles.p, styles.label]}>User</Text>
					<Text style={[TextStyles.p, styles.value]}>{picture.user.first_name} {picture.user.last_name}</Text>
				</View>
				<View style={styles.detail}>
					<Text style={[TextStyles.p, styles.label]}>Likes</Text>
					<Text style={[TextStyles.p, styles.value]}>{picture.likes || 0}</Text>
				</View>
			</View>
		);
	}

	render() {
		const { pictures } = this.props;
		const pictureId = this.props.navigation.getParam('id', null);
		const picture = pictures.records[pictureId];
		return (
			<View style={[LayoutStyles.page, styles.page]}>
				<StatusBar barStyle="light-content" />
                <Icon
                    name="arrow-back"
                    size={24}
                    color={Colors.white}
                    style={styles.backBtn}
                    onPress={() => this.props.navigation.goBack(null)}
                />
                <ScrollView
					refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            tintColor={Colors.primary}
                            progressBackgroundColor={Colors.offWhite}
                            style={{backgroundColor: Colors.offWhite}}
                            onRefresh={this.handleRefresh}
                        />
                    }
				>
					<View style={styles.headerHolder}>
                    	<PictureTile picture={picture} />
					</View>
					{this.renderDetails(picture)}
                </ScrollView>
			</View>
		);
	}
});

const styles = StyleSheet.create({
	page: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    backBtn: {
        position: 'absolute',
        zIndex: 1,
        top: Sizes.gutterLarge * 2,
        padding: Sizes.gutter,
        textAlign: 'left',
        alignSelf: 'flex-start',
    },
	headerHolder: {
		height: 400,
	},
	details: {
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'wrap',
	},
	loaderHolder: {
		padding: Sizes.gutter * 2,
		width: '100%',
		justifyContent: 'center',
	},
	detail: {
		width: '50%',
		padding: Sizes.gutter * 2,
	},
	label: {
		color: Colors.greyLight,
	},
	value: {
		fontSize: 24,
		lineHeight: 29,
	},
});
