import React, { Component } from 'react';
import { View, ScrollView, RefreshControl, StyleSheet, ActivityIndicator, Animated, Easing, Text, Dimensions, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/MaterialIcons';

// redux
import { getPictures } from '../redux/pictures';

// styles
import LayoutStyles from '../styles/Layout';
import TextStyles from '../styles/Text';
import Colors from '../styles/Colors';
import Sizes from '../styles/Sizes';

// components
import PictureTile from '../components/PictureTile';
import Search from '../components/Search';

const { width } = Dimensions.get('window');
const halfImageWidth = (width / 2) - (Sizes.gutter / 2);

export default withNavigationFocus(connect(
	state => ({
		pictures: state.pictures,
	}), {
		getPictures,
	}
)(class HomeScreen extends Component {

	constructor(props) {
		super(props)

		this.state = {
			activeTab: 'daily',
			activePictureId: null,
			heightPictureTile: new Animated.Value(200),
			widthPictureTile: new Animated.Value(halfImageWidth),
			paddingPictureTile: new Animated.Value(Sizes.gutter / 2),
			radiusPictureTile: new Animated.Value(8),
			opacityPictureTile: new Animated.Value(1),
			headerTop: new Animated.Value(0),
			paddingPictures: new Animated.Value(Sizes.gutter / 2),
			isRefreshing: false,
		};

		this.moveAnimation = new Animated.ValueXY({ x: 0, y: 0 });
		this.pictureTileRefs = {};
	}

	componentWillMount() {
		this.props.getPictures();
	}

	componentDidUpdate(prevProps, prevState) {
		// when navigating back
		if (prevProps.isFocused === false && this.props.isFocused === true) {
			this.animateIn();
		}
	}

	handleRefresh = () => {
		this.props.getPictures().then(() => {
			this.setState({
				isRefreshing: false,
			});
		});
	};

	animateIn() {
		// animate in other elements
		Animated.timing(
			this.state.headerTop,
			{
				toValue: 0,
				duration: 300,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.paddingPictures,
			{
				toValue: Sizes.gutter / 2,
				duration: 150,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.opacityPictureTile,
			{
				toValue: 1,
				duration: 350,
				easing: Easing.linear,
				delay: 100,
			}
		).start();

		// animate prev active on back
		Animated.spring(this.moveAnimation, {
			toValue: {
				x: 0,
				y: 0,
			},
		}).start();

		Animated.timing(
			this.state.widthPictureTile,
			{
				toValue: halfImageWidth,
				duration: 200,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.heightPictureTile,
			{
				toValue: 200,
				duration: 200,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.paddingPictureTile,
			{
				toValue: Sizes.gutter / 2,
				duration: 200,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.radiusPictureTile,
			{
				toValue: 8,
				duration: 300,
				easing: Easing.linear,
			}
		).start();
	}

	onItemPress = (id) => {
		this.setState({
			activePictureId: id,
		});

		// animate other elements out
		Animated.timing(
			this.state.headerTop,
			{
				toValue: -120,
				duration: 300,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.paddingPictures,
			{
				toValue: 0,
				duration: 150,
				easing: Easing.linear,
			}
		).start();

		Animated.timing(
			this.state.opacityPictureTile,
			{
				toValue: 0,
				duration: 350,
				easing: Easing.linear,
			}
		).start();

		// animate pressed item
		Animated.timing(
			this.state.widthPictureTile,
			{
				toValue: width,
				duration: 200,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.heightPictureTile,
			{
				toValue: 400,
				duration: 200,
				easing: Easing.cubic,
			}
		).start();

		Animated.timing(
			this.state.paddingPictureTile,
			{
				toValue: 0,
				duration: 200,
				easing: Easing.linear,
			}
		).start();

		Animated.timing(
			this.state.radiusPictureTile,
			{
				toValue: 0,
				duration: 300,
				easing: Easing.linear,
			}
		).start();

		this.pictureTileRefs[id].measure((fx, fy, width, height, px, py) => {
			Animated.spring(this.moveAnimation, {
				toValue: {
					x: -(px - (Sizes.gutter / 2)),
					y: -(py - (Sizes.gutter / 2)),
				},
				duration: 300,
				delay: 100,
			}).start(() => {
				this.props.navigation.navigate({
					routeName: 'Picture',
					params: {
						id,
						transition: 'none',
					},
				});
			});
        });
	};

	handleStateChange = (name, value) => {
		this.setState({
			[name]: value,
		});
	};

	toggleTab = (name) => {
		this.setState({
			activeTab: name,
		});
	};

	renderContent(pictures) {
		if (!pictures) {
			return (
				<View style={styles.loaderHolder}>
					<ActivityIndicator size="small" color={Colors.primary} />
				</View>
			);
		}

		if (!pictures.length) {
			return (
				<View style={styles.loaderHolder}>
					<Text style={[TextStyles.p, styles.emptyMessage]}>No results</Text>
				</View>
			);
		}

		return (
			<Animated.View style={[styles.pictures, {
				padding: this.state.paddingPictures,
			}]}>
				{pictures.map((picture) => {
					let tileModifier = {};
					let boxModifier = {};
					// if is pressed picture
					if (this.state.activePictureId === picture.id) {
						tileModifier = this.moveAnimation.getLayout();
						tileModifier.width = this.state.widthPictureTile;
						tileModifier.padding = this.state.paddingPictureTile;
						boxModifier.height = this.state.heightPictureTile;
						boxModifier.borderRadius = this.state.radiusPictureTile;
					} else {
						tileModifier = {
							opacity: this.state.opacityPictureTile,
						};
					}

					return (
						<View
							key={picture.id}
							ref={node => { this.pictureTileRefs[picture.id] = node }}
							style={styles.pictureHolder}
						>
							<Animated.View style={[styles.tile, tileModifier]}>
								<Animated.View style={[styles.box, boxModifier]}>
									<PictureTile
										picture={picture}
										onPress={() => this.onItemPress(picture.id)}
									/>
								</Animated.View>
							</Animated.View>
						</View>
					);
				})}
			</Animated.View>
		);
	}

	render() {
		const { pictures } = this.props;

		let filteredPictures = _.filter(pictures.records, {});
		if (this.state.activeTab === 'favs') {
			filteredPictures = _.filter(pictures.records, { isFav: true, });
		}
		return (
			<View style={[LayoutStyles.page, styles.page]}>
				<Animated.View style={[styles.header, {
					transform: [
						{
							translateY: this.state.headerTop,
						},
					],
				}]}>
					<Search />
				</Animated.View>
				<ScrollView
					refreshControl={
						<RefreshControl
							refreshing={this.state.isRefreshing}
							tintColor={Colors.primary}
							progressBackgroundColor={Colors.offWhite}
							style={{backgroundColor: Colors.offWhite}}
							onRefresh={this.handleRefresh}
						/>
					}
				>
					<View style={styles.pageInner}>
						<View style={LayoutStyles.container}>
							<Text style={TextStyles.h1}>{this.state.activeTab === 'favs' ? 'Favorites' : 'Daily Pictures'}</Text>
						</View>
						{this.renderContent(filteredPictures)}
					</View>
				</ScrollView>
				<View style={styles.bottomBar}>
					<TouchableHighlight
						style={styles.tabBtn}
						underlayColor={Colors.white}
						onPress={() => this.toggleTab('daily')}
					>
						<React.Fragment>
							<Icon
								name="search"
								size={30}
								color={this.state.activeTab === 'daily' ? Colors.primary : Colors.offBlack}
								style={styles.tabIcon}
							/>
							<Text style={this.state.activeTab === 'daily' ? [TextStyles.p, styles.tabText, styles.tabTextActive] : [TextStyles.p, styles.tabText]}>Search</Text>
						</React.Fragment>
					</TouchableHighlight>
					<TouchableHighlight
						style={styles.tabBtn}
						underlayColor={Colors.white}
						onPress={() => this.toggleTab('favs')}
					>
						<React.Fragment>
							<Icon
								name="favorite-border"
								size={30}
								color={this.state.activeTab === 'favs' ? Colors.primary : Colors.offBlack}
								style={styles.tabIcon}
							/>
							<Text style={this.state.activeTab === 'favs' ? [TextStyles.p, styles.tabText, styles.tabTextActive] : [TextStyles.p, styles.tabText]}>Favourites</Text>
						</React.Fragment>
					</TouchableHighlight>
				</View>
			</View>
		);
	}
}));

const styles = StyleSheet.create({
	header: {
		position: 'absolute',
		top: 0,
		zIndex: 100,
		width: '100%',
		flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
		paddingTop: Sizes.gutterLarge * 2,
		backgroundColor: Colors.white,
	},
	page: {
		alignItems: 'stretch',
	},
	pageInner: {
		paddingTop: 120 + Sizes.gutter,
		paddingBottom: 50 + Sizes.gutterLarge,
	},
	loaderHolder: {
		padding: Sizes.gutterLarge,
	},
	emptyMessage: {
		textAlign: 'center',
	},
	title: {
		fontSize: 34,
		fontFamily: 'SF Pro Display',
		fontWeight: '600',
		color: Colors.greyDark,
	},
	pictures: {
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'flex-start',
		padding: Sizes.gutter / 2,
	},
	pictureHolder: {
		width: halfImageWidth,
	},
	tile: {
		width: '100%',
		padding: Sizes.gutter / 2,
	},
	box: {
		position: 'relative',
		zIndex: 101,
		height: 200,
		borderRadius: 8,
		overflow: 'hidden',
		backgroundColor: '#f00',
	},
	bottomBar: {
		opacity: 0.8,
		position: 'absolute',
		bottom: 0,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: Colors.white,
	},
	tabBtn: {
		padding: Sizes.gutter,
		width: '50%',
	},
	tabIcon: {
		textAlign: 'center',
	},
	tabText: {
		textAlign: 'center',
		fontSize: 12,
	},
	tabTextActive: {
		color: Colors.primary,
	},
});
