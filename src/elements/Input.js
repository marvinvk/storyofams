import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Text } from 'react-native';
import PropTypes from 'prop-types';

// styles
import Sizes from '../styles/Sizes';
import Colors from '../styles/Colors';

export default class InputField extends Component {

    static propTypes = {
        name: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        label: PropTypes.string,
        labelWhite: PropTypes.bool,
        type: PropTypes.oneOf([
            'none',
            'URL',
            'addressCity',
            'addressCityAndState',
            'addressState',
            'countryName',
            'creditCardNumber',
            'emailAddress',
            'familyName',
            'fullStreetAddress',
            'givenName',
            'jobTitle',
            'location',
            'middleName',
            'name',
            'namePrefix',
            'nameSuffix',
            'nickname',
            'organizationName',
            'postalCode',
            'streetAddressLine1',
            'streetAddressLine2',
            'sublocality',
            'telephoneNumber',
            'username',
            'password',
        ]),
        keyboardType: PropTypes.oneOf([
            'default',
            'number-pad',
            'decimal-pad',
            'numeric',
            'email-address',
            'phone-pad',
        ]),
        autoCapitalize: PropTypes.oneOf([
            'none',
            'characters',
            'words',
            'sentences',
        ]),
        placeholder: PropTypes.string,
        password: PropTypes.bool,
        max: PropTypes.number,
        disabled: PropTypes.bool,
        onChange: PropTypes.func,
        onChangeDelayed: PropTypes.func,
        onSubmitEditing: PropTypes.func,
        error: PropTypes.any,
    };

    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };
    }

    componentDidUpdate(prevProps) {
        // if has new value
        if (prevProps.value !== this.props.value) {
            this.setState({
                value: this.props.value,
            });
        }
    }

    handleChange = (value) => {
        if (this.props.onChange) {
            this.props.onChange(this.props.name, value);
        }

        if (this.props.onChangeDelayed) {
            clearTimeout(this.timer);
            this.timer = setTimeout(() => {
                this.props.onChangeDelayed(this.props.name, value);
            }, 1000);
        }
    }

	render() {
        let inputModifier = {};
        if (this.props.error) {
            inputModifier = styles.inputError;
        }
        if (this.props.disabled) {
            inputModifier = styles.inputDisabled;
        }

        let labelModifier = {};
        if (this.props.labelWhite) {
            labelModifier = styles.labelWhite;
        }

        return (
            <View style={styles.inputHolder}>
                {this.props.label &&
                    <Text style={[styles.label, labelModifier]}>{this.props.label}</Text>
                }
                <View style={[styles.input, inputModifier]}>
                    <TextInput
                        name={this.props.name}
                        value={this.state.value}
                        style={styles.field}
                        textContentType={this.props.type}
                        placeholder={this.props.placeholder}
                        secureTextEntry={this.props.password}
                        keyboardType={this.props.keyboardType}
                        maxLength={this.props.max}
                        autoCapitalize={this.props.autoCapitalize}
                        onChangeText={this.handleChange}
                        editable={!this.props.disabled}
                        onSubmitEditing={this.props.onSubmitEditing}
                    />
                </View>
                {this.props.error &&
                    <Text style={styles.error}>{this.props.error}</Text>
                }
            </View>
        );
	}
}

const styles = StyleSheet.create({
    inputHolder: {
        width: '100%',
    },
    label: {
        fontSize: 14,
		color: Colors.offBlack,
		marginVertical: Sizes.gutter / 2,
    },
    labelWhite: {
        color: Colors.white,
    },
	input: {
		fontSize: 14,
        lineHeight: Sizes.buttonHeight,
        color: Colors.greyDark,
        textAlign: 'left',
        width: '100%',
        height: Sizes.buttonHeight,
        paddingHorizontal: Sizes.gutter,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: Colors.grey,
        backgroundColor: Colors.grey,
        borderRadius: Sizes.buttonHeight / 2,
    },
    inputError: {
        borderColor: Colors.error,
    },
    inputDisabled: {
        opacity: 0.5,
        backgroundColor: Colors.greyXLight,
    },
    field: {
        height: Sizes.buttonHeight,
    },
    error: {
        color: Colors.error,
        fontSize: 14,
        paddingTop: Sizes.gutter / 2,
	},
});
