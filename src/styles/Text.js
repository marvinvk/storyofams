import { StyleSheet } from 'react-native';

import Colors from './Colors';

module.exports = StyleSheet.create({
	h1: {
        fontFamily: 'SF Pro Display',
		fontSize: 34,
		fontWeight: '700',
		color: Colors.greyDark,
    },
    p: {
        fontFamily: 'SF Pro Display',
        fontSize: 16,
        lineHeight: 24,
        color: Colors.offBlack,
    }
});
