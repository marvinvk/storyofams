module.exports = {
	primary: '#1B3CEA',

	// grayscales
	white: '#ffffff',
	offWhite: '#E5E5E5', // done
	greyLight: '#9AA5B1',
	grey: '#DBE0E6', // done
	greyDark: '#323F4B',
	offBlack: '#1F2933', // done
	black: '#000000', // done

	// status
	error: '#e34036',

	redDark: '#ce3d34',
	red: '#ff5a5a',
	redLight: '#e86b63',

	green: '#35a229',
};
