import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

import Colors from './Colors';
import Sizes from './Sizes';

module.exports = StyleSheet.create({
	page: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		position: 'relative',
		backgroundColor: Colors.white,
	},
	container: {
		width,
		maxWidth: 500,
		paddingHorizontal: Sizes.gutter,
	},
	spacing: {
		padding: Sizes.gutter,
		backgroundColor: Colors.white,
	},
});
