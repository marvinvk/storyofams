import { combineReducers } from 'redux';

import pictures from './redux/pictures';

const reducers = combineReducers({
    pictures,
});

export default reducers;
