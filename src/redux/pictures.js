import _ from 'lodash';

export const GET_PICTURES = 'GET_PICTURES';
export const GET_PICTURES_SUCCESS = 'GET_PICTURES_SUCCESS';
export const GET_PICTURES_FAIL = 'GET_PICTURES_FAIL';

export const GET_PICTURE = 'GET_PICTURE';
export const GET_PICTURE_SUCCESS = 'GET_PICTURE_SUCCESS';
export const GET_PICTURE_FAIL = 'GET_PICTURE_FAIL';

export const SET_FAV = 'SET_FAV';

export default function reducer(state = { repos: [] }, action) {
	// trick to make an immutable state
	const newState = _.cloneDeep(state);

	switch (action.type) {
		case GET_PICTURES:
			return {
				...state,
				loading: true,
			};
		case GET_PICTURES_SUCCESS:
			// reformat the data with keys to match easy later on
			const pictures = {};
			const data = action.payload.data.results || action.payload.data;
			data.forEach((picture) => {
				pictures[picture.id] = picture;
			});

			return {
				...state,
				loading: false,
				records: pictures,
			};
		case GET_PICTURES_FAIL:
			return {
				...state,
				loading: false,
				error: 'Error getting pictures',
			};

		case GET_PICTURE:
			return {
				...state,
				loading: true,
			};
		case GET_PICTURE_SUCCESS:
			let picturesWithDetails = {};
			if (state.records) {
				picturesWithDetails = newState.records;
			}

			// when already has records in the store
			if (state.records && action.payload.data) {
				// find match record
				const matchingRecord = picturesWithDetails[action.payload.data.id];

				// when found match add new fields
				if (matchingRecord) {
					Object.keys(action.payload.data).forEach((key) => {
						matchingRecord[key] = action.payload.data[key];
					});
				} else {
					// otherwise just add the data from the response
					picturesWithDetails[action.payload.data.id] = action.payload.data;
				}
			} else {
				// otherwise just add the data from the response
				picturesWithDetails[action.payload.data.id] = action.payload.data;
			}

			return {
				...state,
				loading: false,
				records: picturesWithDetails,
				error: '',
			};

		case SET_FAV:
			let picturesWithFav = {};
			if (state.records) {
				picturesWithFav = newState.records;
			}

			// find matching record
			const matchingRecord = picturesWithFav[action.id];
			matchingRecord.isFav = !matchingRecord.isFav;

			return {
				...state,
				records: picturesWithFav,
			};

		default:
			return state;
	}
}

export function getPictures(query) {
	let url = 'photos';
	if (query) {
		url = `/search/photos?query=${query}`;
	}

	return {
		type: GET_PICTURES,
		query,
		payload: {
			request: {
				url,
				method: 'GET',
			},
		},
	};
}

export function getPicture(id) {
	return {
		type: GET_PICTURE,
		payload: {
			request: {
				url: `/photos/${id}`,
				method: 'GET',
			},
		},
	};
}

export function setFav(id) {
	return {
		type: SET_FAV,
		id,
		payload: {},
	};
}
