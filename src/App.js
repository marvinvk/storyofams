import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { multiClientMiddleware } from 'redux-axios-middleware';
import thunk from 'redux-thunk';
import axios from 'axios';

import reducers from './Reducers.js';
import Navigation from './Navigation.js';

AsyncStorage.getItem('access_token').then((token) => {
	if (token) {
		// put token in Axios header
		axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
	}
});

const store = createStore(
	reducers,
	applyMiddleware(multiClientMiddleware({
		default: {
			client: axios.create({
				baseURL: 'https://api.unsplash.com',
				responseType: 'json',
				headers: {
					'Accept-Version': 'v1',
					Authorization: 'Client-ID 05409c48cfc6e6392be07325826046f91c0f42ee76c859cd3a09986ffaee6550',
				},
			}),
		},
	}), thunk),
);

// for chrome debugger
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;
global.FormData = global.originalFormData

export default class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Navigation />
			</Provider>
		);
	}
}
