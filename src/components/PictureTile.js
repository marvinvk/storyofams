import React, { Component } from 'react';
import { StyleSheet, TouchableHighlight, Image, View } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';

// redux
import { setFav } from '../redux/pictures';

// styles
import Colors from '../styles/Colors';
import Sizes from '../styles/Sizes';

export default connect(
	state => ({
		pictures: state.pictures,
	}), {
		setFav,
	}
)(class PictureTileComponent extends Component {

	handleFavClick = (id) => {
		this.props.setFav(id);
	};

	render() {
		const { picture } = this.props;
		return (
			<View style={styles.box}>
				<TouchableHighlight
					activeOpacity={1}
					underlayColor={Colors.white}
					onPress={this.props.onPress}
				>
					<Image
						source={{ uri: picture.urls.regular }}
						style={styles.image}
					/>
				</TouchableHighlight>
				<Icon
					name={picture.isFav ? 'favorite' : 'favorite-border'}
					size={20}
					color={picture.isFav ? Colors.primary :  Colors.offBlack}
					style={styles.favIcon}
					onPress={() => this.handleFavClick(picture.id)}
				/>
			</View>
		);
	}
});

const styles = StyleSheet.create({
	box: {
		position: 'relative',
		justifyContent: 'flex-end',
		backgroundColor: Colors.white,
		overflow: 'hidden',
	},
	image: {
		minHeight: 200,
		height: '100%',
    	resizeMode: 'cover',
        backgroundColor: Colors.offBlack,
	},
	favIcon: {
		position: 'absolute',
		right: Sizes.gutter,
		bottom: Sizes.gutter,
		width: 36,
		height: 36,
		textAlign: 'center',
		lineHeight: 36,
		borderRadius: 18,
		backgroundColor: Colors.white,
	},
});
