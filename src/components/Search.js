import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/MaterialIcons';

// redux
import { getPictures } from '../redux/pictures';

// styles
import Colors from '../styles/Colors';
import Sizes from '../styles/Sizes';

// elements
import Input from '../elements/Input';

export default withNavigationFocus(connect(
	state => ({
		pictures: state.pictures,
	}), {
		getPictures,
	}
)(class SearchComponent extends Component {

    constructor(props) {
		super(props)

		this.state = {
			search: '',
		};
	}

    handleStateChange = (name, value) => {
		this.setState({
			[name]: value,
		});
    };

    handleSearch = (name, value) => {
        this.props.getPictures(value);
    };

    clearField = (name) => {
        this.setState({
			[name]: '',
        });
        this.props.getPictures();
    };

    renderIcon() {
        if (this.state.search) {
            return (
                <Icon
                    name="clear"
                    size={24}
                    color={Colors.offBlack}
                    style={styles.searchIcon}
                    onPress={this.clearField}
                />
            );
        }

        return (
            <Icon
                name="search"
                size={24}
                color={Colors.offBlack}
                style={styles.searchIcon}
                onPress={this.handleSearch}
            />
        );
    }

	render() {
		return (
            <View style={styles.search}>
                <Input
                    style={styles.input}
                    name="search"
                    placeholder="Search Pictures"
                    autoCapitalize="none"
                    value={this.state.search}
                    onChange={this.handleStateChange}
                    onChangeDelayed={this.handleSearch}
                />
                <Icon
                    name={this.state.search ? 'clear' : 'search'}
                    size={24}
                    color={Colors.offBlack}
                    style={styles.searchIcon}
                    onPress={this.state.search ? () => this.clearField('search') : () => this.handleSearch('search', this.state.search)}
                />
            </View>
		);
	}
}));

const styles = StyleSheet.create({
	search: {
		position: 'relative',
		width: '100%',
		padding: Sizes.gutter,
	},
	searchIcon: {
		position: 'absolute',
		top: 24,
		right: Sizes.gutter * 2,
	},
});
